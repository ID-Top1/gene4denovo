#!/bin/bash

echo "Usage: $0 [/path/to/GRCh37.fa] [gene4denovo_url]"
ref_fasta=$1
g4d_url=$2
source_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ "$ref_fasta" == "" ]
then
    #retrieve reference genome
    wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz
    gzip -d hs37d5.fa.gz
    samtools faidx hs37d5.fa
    ref_fasta=hs37d5.fa
fi
if [ "$g4d_url" == "" ]
then
    g4d_url=http://www.genemed.tech/gene4denovo/uploads/gene4denovo/All_De_novo_mutations_1.2.txt
fi
# Retrieve gene4denovo file
wget $g4d_url

# calculate number of DNMs per WGS sample
grep -w WGS All_De_novo_mutations_1.2.txt | \
    cut -f 6  | sort | uniq -c > wgs_individual_dnm_counts.txt

# Identify WGS samples with more than 33 and less than 140 putative DNMs
# Cutoff chosen as per jupyter-notebook "dnm_counts.ipynb"
perl -wanE 'say $F[1] if $F[0] > 33 and $F[0] < 140;' \
    wgs_individual_dnm_counts.txt > wgs_individuals_34to139_variants.txt

# Convert to VCF with variants from selected WGS samples for sigProfiler
"${source_dir}/convert_gene4denovo.py" All_De_novo_mutations_1.2.txt \
                                    ${ref_fasta} \
                                    -l wgs_individuals_34to139_variants.txt \
                                    -o gene4denovo_wgs_selected.bcf \
                                    -e gene4denovo_wgs_selected.errors.txt

# normalize indels
bcftools norm -f ${ref_fasta} \
    -O b \
    -o gene4denovo_wgs_selected_norm.bcf \
    gene4denovo_wgs_selected.bcf

