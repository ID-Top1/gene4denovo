#!/usr/bin/env python3
import argparse
import csv
import gzip
import logging
import pysam
import os
import sys
from pyfaidx import Fasta
from random import randint

logger = logging.getLogger('Converter')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def get_outfile(fasta, f=None, samples=0, sample_list=[]):
    if f is None:
        f = '-'
    vcf = pysam.VariantFile(f, mode='w')
    vcf.header.add_meta(key=os.path.basename(sys.argv[0]),
                        value=' '.join(sys.argv[1:]))
    vcf.header.add_meta(key='source', value=os.path.basename(sys.argv[0]))
    vcf.header.add_meta(key='gene4denovo', value=f)
    vcf.header.add_meta(key='reference', value=fasta.filename)
    for contig in fasta.keys():
        vcf.header.contigs.add(contig)
    vcf.header.info.add('seq_type', 1, 'String',
                        'DNM originated from WES or WGS data')
    vcf.header.info.add('study_id', 1, 'String', 'Gene4DeNovo Study ID')
    vcf.header.info.add('paper', 1, 'String', 'Paper identifying DNM')
    vcf.header.info.add('pmid', 1, 'String',
                        'PubMed ID of paper identifying DNM')
    vcf.header.info.add('dnm_group', 1, 'String', 'gene4denovo grouping')
    if samples > 0 or sample_list:
        vcf.header.formats.add("GT", 1, "String", "Genotype")
    for i in range(samples):
        vcf.header.samples.add("Sample{}".format(i + 1))
    for sid in sample_list:
        vcf.header.samples.add(sid)
    return vcf


def parse_row_alleles(row, fasta):
    start = int(row['start'])
    if row['ref'] == '-' or row['alt'] == '-':  # indel
        pos = start - 1
        seq_prefix = fasta[row['chrom']][pos - 1:pos]
        if row['ref'] == '-':  # insertion
            ref = seq_prefix
            alt = seq_prefix + row['alt']
        else:  # deletion
            ref = seq_prefix + row['ref']
            alt = seq_prefix
    else:  # SNV/MNV
        pos = start
        ref = row['ref']
        alt = row['alt']
    return pos, ref, alt


def get_errors_file(f, columns):
    if f is None:
        return None, None
    if f.endswith('.gz'):
        fh = gzip.open(f, 'wt', newline='')
    else:
        fh = open(f, 'wt', newline='')
    fieldnames = columns + ('reason', )
    return csv.DictWriter(fh, fieldnames=fieldnames), fh


def write_error(csv_fh, row, msg):
    if csv_fh is None:
        return
    row['reason'] = msg
    csv_fh.writerow(dict((k, row[k]) for k in csv_fh.fieldnames))


def check_variant(row, faidx, err_file):
    if row['chrom'] not in faidx:  # ignore non-standard contigs
        write_error(err_file, row, 'Chromosome not in reference FASTA')
        return 1
    start = int(row['start']) - 1
    span = start + len(row['ref'])
    chrom_length = len(faidx.records[row['chrom']])
    if span > chrom_length:
        write_error(
            err_file, row,
            "Position beyond chromosome length ({})".format(chrom_length))
        return 1
    if row['ref'] != '-':
        fasta_seq = faidx[row['chrom']][start:span]
        if fasta_seq != row['ref']:
            write_error(
                err_file, row,
                "REF allele '{}' does not match ".format(row['ref']) +
                "FASTA reference '{}'".format(fasta_seq))
            return 1
    return 0


def read_sample_ids_from_file(f):
    samples = set()
    if f is None:
        return samples
    with open(f, 'rt') as fh:
        for line in fh:
            s_id = line.strip().split()
            if s_id:
                samples.add(s_id[0])
    return samples


def txt2vcf(mutations,
            fasta,
            output=None,
            errors=None,
            samples=0,
            id_list=None,
            progress_interval=100_000):
    '''
    Convert annotations from gene4denovo "All De novo mutations file":
       http://www.genemed.tech/gene4denovo/uploads/All_De_novo_mutations_1.2.txt
    '''
    cols = ('chrom', 'start', 'end', 'ref', 'alt', 'study_id', 'dnm_group',
            'seq_type', 'paper', 'pmid')
    info_cols = ('dnm_group', 'study_id', 'seq_type', 'paper', 'pmid')
    faidx = Fasta(fasta, sequence_always_upper=True, as_raw=True)
    sample_ids = read_sample_ids_from_file(id_list)
    vcf = get_outfile(faidx, output, samples=samples, sample_list=sample_ids)
    err_csv, err_fh = get_errors_file(errors, cols)
    read, written, errors = 0, 0, 0
    o_func = open
    if mutations.endswith('.gz'):
        o_func = gzip.open
    with o_func(mutations, 'rt', newline='') as infile:
        tsv = csv.DictReader(infile, fieldnames=cols, delimiter='\t')
        for row in tsv:
            read += 1
            if progress_interval > 0 and read % progress_interval == 1:
                if read != 1:
                    logger.info(
                        "{:,} processed, {:,} written, {:,} errors...".format(
                            read - 1, written, errors))
            try:
                if sample_ids and row['study_id'] not in sample_ids:
                    continue
                # occasional alleles have extraneous whitespace
                row['ref'] = row['ref'].replace(' ', '')
                row['alt'] = row['alt'].replace(' ', '')
                e = check_variant(row, faidx, err_csv)
                if e:
                    errors += 1
                    continue
                rec = vcf.new_record()
                pos, ref, alt = parse_row_alleles(row, faidx)
                rec.chrom = row['chrom']
                rec.pos = pos
                rec.ref = ref
                rec.alts = (alt, )
                for x in info_cols:
                    rec.info[x] = row[x].replace(' ', '_')
                if samples > 0:
                    i = randint(1, samples)
                    rec.samples['Sample{}'.format(i)]['GT'] = (0, 1)
                if sample_ids:
                    rec.samples[row['study_id']]['GT'] = (0, 1)
                vcf.write(rec)
                written += 1
            except Exception as e:
                errors += 1
                write_error(err_csv, row, "Unknown error")
                logger.exception(e)
    vcf.close()
    if err_fh is not None:
        err_fh.close()
    logger.info(
        "Finished processing {:,} records, {:,} written, {:,} errors.".format(
            read, written, errors))


def get_options():
    parser = argparse.ArgumentParser(
        description='''Convert gene4denovo text file into a VCF/BCF file''')
    parser.add_argument("mutations",
                        help='''Text file from gene4denovo - i.e. the following
                        file:
                        http://www.genemed.tech/gene4denovo/uploads/''' +
                        'All_De_novo_mutations_1.2.txt')
    parser.add_argument("fasta",
                        help='''Reference FASTA file. Must match the
                        gene4denovo reference build (i.e. GRCh37).''')
    parser.add_argument("-o",
                        "--output",
                        help='''Output filename. Defaults to STDOUT if not
                        provided. If a filename is provided output format will
                        be inferred from extension ('.vcf.gz' for bgzip
                        compressed VCF, '.bcf' for BCF format, uncompressed VCF
                        for anything else).''')
    parser.add_argument("-e",
                        "--errors",
                        help='''Write errors to this file. Lines that can not
                        be correctly parsed from your input file will be
                        written to this file with an additional column with the
                        reason records could not be parsed. Add a '.gz' suffix
                        to have this output written in gzip compressed
                        format.''')
    sample_group = parser.add_mutually_exclusive_group()
    sample_group.add_argument("-s",
                              "--samples",
                              metavar='N',
                              type=int,
                              default=0,
                              help='''Number of samples to randomly
                              assign variants to. By default no samples are
                              added to the VCF.''')
    sample_group.add_argument("-l",
                              "--id_list",
                              metavar='FILE',
                              help='''Provide a file of sample IDs
                              (corresponding to column 6 of the standard
                              gene4denovo file) to extract variants for.
                              These will be added as samples to the VCF and
                              variants from other samples will be ignored. Only
                              the first non-whitespace string will be read from
                              each line.''')
    parser.add_argument("--progress_interval",
                        metavar='N',
                        type=int,
                        default=100_000,
                        help='Report progress every N records. Default=100000')
    return parser


if __name__ == '__main__':
    parser = get_options()
    args = parser.parse_args()
    txt2vcf(**vars(args))
