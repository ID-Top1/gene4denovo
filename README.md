# gene4denovo to VCF

Code for parsing data from Gene4Denovo (http://www.genemed.tech/gene4denovo/)
and performing mutation signature analysis.

## Requirements

Preparation of VCFs for signature analysis requires
[bcftools](https://samtools.github.io/bcftools/bcftools.html).

Preparation of reference genome FASTA requires [samtools](https://github.com/samtools/samtools)

VCF conversion requires python3 and the modules `pysam` and `pyfaidx`.
Required python modules can be installed via pip.

## Usage:

To convert gene4denovo text file to VCF as per analyses in doi:xxx/xxx:

    # Clone this repository to your working directory
    git clone https://git.ecdf.ed.ac.uk/Deletions_paper/gene4denovo.git

    # Run the wrapper script
    ./gene4denovo/extract_wgs_variants.sh

This will output a file called 'gene4denovo_wgs_selected_norm.bcf' containing DNMs from samples sequenced by WGS (not WES) and with a minimum of 33 DNMs and maximum of 140 DNMs. Variants will be left-aligned and normalized.

More generally, if you want to convert variants from Gene4Denovo for other purposes, you will require the GRCh37 reference FASTA sequence:

    # Retrieve reference FASTA if you don't already have it
    wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz
    gzip -d hs37d5.fa.gz
    samtools faidx hs37d5.fa

And the full list of de novo variants from Gene4Denovo:

    # Retrieve gene4denovo file
    wget http://www.genemed.tech/gene4denovo/uploads/gene4denovo/All_De_novo_mutations_1.2.txt

Run `convert_gene4denovo.py -h` to see instructions on how to run and the available options.

## AUTHOR

Written by David A. Parry at the University of Edinburgh.

## COPYRIGHT AND LICENSE

MIT License

Copyright (c) 2021 David A. Parry

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
