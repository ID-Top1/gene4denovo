#!/usr/bin/env python3
import argparse
import logging
import pysam


logger = logging.getLogger('Subset')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def output_samples(vcf, prefix, samples, extension='.vcf',
                   progress_interval=10_000):
    out_files = dict()
    with pysam.VariantFile(vcf) as variants:
        variants.subset_samples(samples)
        for smp in samples:
            fn = prefix + smp + extension
            out_files[smp] = pysam.VariantFile(fn, mode='w')
            for record in variants.header.records:  # add all except col header
                out_files[smp].header.add_record(record)
            out_files[smp].header.samples.add(smp)
        read, written = 0, 0
        for record in variants:
            read += 1
            if read % progress_interval == 0:
                logger.info("Read {:,} variants, wrote {:,}".format(read,
                                                                    written))
            for smp, fh in out_files.items():
                if any(x is not None and x > 0 for x in
                       record.samples[smp]['GT']):
                    new_rec = out_files[smp].header.new_record(
                        contig=record.contig,
                        start=record.start,
                        stop=record.stop,
                        alleles=record.alleles,
                        id=record.id,
                        qual=record.qual,
                        filter=record.filter,
                        info=record.info)
                    for k, v in record.samples[smp].items():
                        new_rec.samples[smp][k] = v
                    out_files[smp].write(new_rec)
                    written += 1
    variants.close()
    for k, fh in out_files.items():
        fh.close()


def subset_vcf_samples(vcf, output_prefix, max_open=1000, fileformat='vcf',
                       progress_interval=10_000):
    with pysam.VariantFile(vcf) as variants:
        samples = list(variants.header.samples)
    extension = '.' + fileformat.lower()
    p = 0
    for i in range(0, len(samples), max_open):
        p += 1
        subset = samples[i:i+max_open]
        logger.info("Processing {:,} samples in pass {:,}".format(len(subset),
                                                                  p))
        output_samples(vcf, output_prefix, samples=subset, extension=extension,
                       progress_interval=progress_interval)
    logger.info("Finished outputting VCFs for {:,} samples".format(
        len(samples)))


def get_options():
    parser = argparse.ArgumentParser(
        description='''Write per-sample VCF/BCFs from a multi-sample VCF''')
    parser.add_argument("vcf", help='Input multi-sample VCF file')
    parser.add_argument("output_prefix", help='''Prefix for output files. Each
                        file will be named "<prefix><sample>.<fileformat>". If
                        using a directory name for your prefix ensure that the
                        directory already exists!''')
    parser.add_argument("-f", "--fileformat", default='vcf', help='''Output
                        format. Specify "vcf", "vcf.gz" or "bcf" to write
                        output files in appropriate format. Default=vcf.''')
    parser.add_argument("-m", "--max_open", metavar='N', type=int,
                        default=1000, help='''Maximum files to write in one
                        pass. May be limited be your OS. Default=1000''')
    parser.add_argument("--progress_interval", metavar='N', type=int,
                        default=10_000,
                        help='Report progress every N records. Default=10000')
    return parser


if __name__ == '__main__':
    parser = get_options()
    args = parser.parse_args()
    subset_vcf_samples(**vars(args))
