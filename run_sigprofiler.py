#!/usr/bin/env python3
import sys
from SigProfilerExtractor import sigpro as sig


def main(input_dir, output_dir, ref):
    sig.sigProfilerExtractor("vcf",
                             output_dir,
                             input_dir,
                             reference_genome=ref)


if __name__ == '__main__':
    if len(sys.argv) != 4:
        sys.exit("Usage: {} vcf_dir out_dir reference_genome".format(
            sys.argv[0]))
    main(*sys.argv[1:])
