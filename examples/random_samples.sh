#!/bin/bash

git clone https://git.ecdf.ed.ac.uk/RER_deletions_paper/gene4denovo.git

# Retrieve reference FASTA if you don't already have it
wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz
gzip -d hs37d5.fa.gz
samtools faidx hs37d5.fa

# Retrieve gene4denovo file
wget http://www.genemed.tech/gene4denovo/uploads/All_De_novo_mutations_1.2.txt

# Convert to VCF with variants randomly assigned to 20 samples
./gene4denovo/convert_gene4denovo.py All_De_novo_mutations_1.2.txt \
    hs37d5.fa \
    -s 20 \
    -o gene4denovo.vcf.gz \
    -e errors.txt

# Optionally sort and normalize indels
bcftools sort -O u gene4denovo.vcf.gz | \
    bcftools norm -f hs37d5.fa -O z -o gene4denovo.norm.vcf.gz

# Create per-sample input files for SigProfiler
mkdir sig_profiler_input
for sample in `bcftools query -l gene4denovo.norm.vcf.gz`
do 
    bcftools view -U -s ${sample} -O v -o sig_profiler_input/${sample}.vcf gene4denovo.norm.vcf.gz
done

# Run SigProfiler 
./gene4denovo/run_sigprofiler.py sig_profiler_input/ sig_profiler_output/ GRCh37

