#!/bin/bash

git clone https://git.ecdf.ed.ac.uk/RER_deletions_paper/gene4denovo.git

# Retrieve reference FASTA if you don't already have it
wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz
gzip -d hs37d5.fa.gz
samtools faidx hs37d5.fa

# Retrieve gene4denovo file
wget http://www.genemed.tech/gene4denovo/uploads/All_De_novo_mutations_1.2.txt

# Identify WGS samples with more than 50 and fewer than 200 putative DNMs
grep -w WGS All_De_novo_mutations_1.2.txt | \
    cut -f 6  | sort | uniq -c > wgs_individual_dnm_counts.txt
perl -wanE 'say $F[1] if $F[0] > 50 and $F[0] < 200;' \
     wgs_individual_dnm_counts.txt > wgs_individuals_gt50_lt200_variants.txt

# Convert to VCF with variants from selected WGS samples
./gene4denovo/convert_gene4denovo.py All_De_novo_mutations_1.2.txt \
                                     hs37d5.fa \
                                     -l wgs_individuals_gt50_lt200_variants.txt \
                                     -o gene4denovo_wgs_selected.vcf.gz \
                                     -e gene4denovo_wgs_selected.errors.txt

# create per sample VCFs for sigprofiler
mkdir -p per_sample_vcfs/
./gene4denovo/subset_vcf_on_sample.py gene4denovo_wgs_selected.vcf.gz \
                                      per_sample_vcfs/

# Run SigProfiler 
./gene4denovo/run_sigprofiler.py per_sample_vcfs/ sig_profiler_output/ GRCh37
